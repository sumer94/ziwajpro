var express = require('express');
var router = express.Router();

/* GET sign_in listing. */
router.get('/', function(req, res, next) {
    res.render('sign_in', { title: 'Express' , name: 'Hamza' });
});

module.exports = router;
