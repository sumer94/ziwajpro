var express = require('express');
var router = express.Router();

/* GET total_pages listing. */
router.get('/', function(req, res, next) {
    res.render('total_pages', { title: 'Express' , name: 'Hamza' });
});
module.exports = router;
