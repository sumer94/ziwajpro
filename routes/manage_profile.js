var express = require('express');
var router = express.Router();

/* GET manage_profile listing. */
router.get('/', function(req, res, next) {
    res.render('manage_profile', { title: 'Express' , name: 'Hamza' });
});
module.exports = router;
