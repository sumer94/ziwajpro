var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var sign_in = require('./routes/sign_in');
var sign_up = require('./routes/sign_up');
var forgot_password = require('./routes/forgot_password');
var dashboard = require('./routes/dashboard');
var add_profile = require('./routes/add_profile');
var manage_profile = require('./routes/manage_profile');
var total_pages = require('./routes/total_pages');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/sign_in', sign_in);
app.use('/sign_up', sign_up);
app.use('/forgot_password', forgot_password);
app.use('/dashboard', dashboard);
app.use('/add_profile', add_profile);
app.use('/manage_profile', manage_profile);
app.use('/total_pages', total_pages);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
